FROM ubuntu:22.04
MAINTAINER rleigh@codelibre.net

RUN apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y -qq --no-install-recommends \
    build-essential \
    cmake \
    curl \
    doxygen \
    git \
    jq \
    man \
    locales \
    libglm-dev \
    graphviz \
    zip \
    unzip \
    libgtest-dev \
    libboost-date-time-dev \
    libboost-dev \
    libboost-filesystem-dev \
    libboost-graph-dev \
    libboost-iostreams-dev \
    libboost-log-dev \
    libboost-program-options-dev \
    libboost-random-dev \
    libboost-regex-dev \
    libfmt-dev \
    libhdf5-dev \
    ninja-build \
    libpng-dev \
    python3.10 \
    python3-genshi \
    python3-flake8 \
    python3-sphinx \
    python3-six \
    python3-venv \
    libpython3.10-dev \
    libqt6opengl6-dev \
    libqt6svg6-dev \
    libqt6core5compat6-dev \
    libspdlog-dev \
    libsqlite3-dev \
    sqlite3 \
    libtiff5-dev \
    libxalan-c-dev \
    libxerces-c-dev \
    zlib1g-dev \
  && locale-gen en_US.UTF-8

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
